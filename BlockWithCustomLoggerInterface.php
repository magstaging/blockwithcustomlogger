<?php

namespace Mbs\BlockWithCustomLogger;

interface BlockWithCustomLoggerInterface
{
    /**
     * @return string
     */
    public function getBlockMessage();
}