# README #

Implement a custom logging feature so that block can
log their own activity in their dedicated log file using a definition 
in di.xml that fallback on a generic log file

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/BlockWithCustomLogger when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

Log any page, the rendering of 3 blocks should be made. Also, 3 different log file should be created with a custom block message in their respective log file