<?php

namespace Mbs\BlockWithCustomLogger\Block;

use Magento\Framework\View\Element\Template;

class ARandomBlock extends \Magento\Framework\View\Element\Template
    implements \Mbs\BlockWithCustomLogger\BlockWithCustomLoggerInterface
{
    private $customLogger;

    public function __construct(
        Template\Context $context,
        \Mbs\BlockWithCustomLogger\CustomLoggerInterface $customLogger,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customLogger = $customLogger;
    }

    protected function _toHtml()
    {
        /** @var \Mbs\BlockWithCustomLogger\LoggerGeneric $logger */
        $logger = $this->customLogger;
        $logger->addLog($this->getBlockMessage());

        return parent::_toHtml();
    }

    /**
     * @return string
     */
    public function getBlockMessage()
    {
        return sprintf('%s is getting rendered', get_class($this));
    }
}