<?php

namespace Mbs\BlockWithCustomLogger;

interface CustomLoggerInterface
{
    /**
     * @param string $message
     */
    public function addLog($message);
}